#!/bin/bash

PYTHON=python
SETUP=setup.py
UPTOOL=twine

VERSION := $(shell cat VERSION)
DIST_FILES := \
	dist/csb43-$(VERSION)-py2.py3-none-any.whl \
	dist/csb43-$(VERSION).tar.gz

.PHONY: clean-version upload doc upload-doc

all: flake8

flake8-py2: flake8.awk
	`which python2` -m flake8 . | awk -f $< >&2

flake8-py3: flake8.awk
	`which python3` -m flake8 . | awk -f $< >&2

flake8: flake8.awk
	flake8 . | awk -f $< >&2

test: $(SETUP) tox.ini
	tox

test-rebuild: $(SETUP) tox.ini
	tox -r

clean: clean-pyco clean-cache clean-pycache

clean-pyco:
	@-find . -name "*.py[co]" -type f -delete

clean-cache:
	@-find . -name ".cache" -type d -ls -exec rm -rv {} \;

clean-pycache:
	@-find . -name "__pycache__" -type d -ls -exec rm -rv {} \;

clean-dist:
	-mv dist/* dist.old/

clean-version:
	-rm -fr build/
	-rm -fr csb43.egg-info/

sdist: $(SETUP) clean clean-version
	$(PYTHON) $< sdist

bdist: $(SETUP) clean clean-version
	$(PYTHON) $< bdist bdist_wheel

doc: $(SETUP)
	$(PYTHON) $< build_sphinx

upload-doc: $(SETUP) doc
	$(PYTHON) $< upload_docs

dist: test-rebuild clean-version clean-dist sdist bdist

dist/csb43-$(VERSION)-py2.py3-none-any.whl: bdist

dist/csb43-$(VERSION).tar.gz: sdist

upload: $(SETUP) test-rebuild $(DIST_FILES)
	$(UPTOOL) upload $(DIST_FILES)
	
ofx: Makefile.ofx
	$(MAKE) -f $<
