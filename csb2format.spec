# -*- mode: python -*-
a = Analysis([os.path.join('.','csb2format')],
             pathex=[os.path.join('.','pyinstaller')],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
##### include mydir in distribution #######
def extra_datas(mydir):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % mydir, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'DATA'))

    return extra_datas
###########################################
# append the 'data' dir
a.datas += extra_datas('csb43/i18n')

#_pycountry_files = [
    #'iso3166.xml',
    #'iso3166_2.xml',
    #'iso15924.xml',
    #'iso4217.xml',
    #'iso639.xml'
    #]

#_PYCOUNTRY_PATH = os.path.join('pycountry','databases')
#_PYCOUNTRY_LOCATION = '../ENV/lib/python2.7/site-packages/pycountry-0.14.8-py2.7.egg/pycountry/databases'

#pycountry_f = [(os.path.join(_PYCOUNTRY_PATH, x), os.path.join(_PYCOUNTRY_LOCATION, x), 'DATA') for x in _pycountry_files]

pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=os.path.join('dist', 'csb2format.exe'),
          debug=False,
          strip=None,
          upx=True,
          onefile=True,
          console=True )

#coll = COLLECT(
    #exe,
    #pycountry_f,
    #upx=True,
    #onefile=True,
    #name='distFinal')

