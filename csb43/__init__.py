'''Spanish banks' CSB norm 43 converter to OFX, Homebank, json, yaml, xls, xlsx, ods, csv, tsv'''

__version__ = '0.8.4'
__all__ = ['csb43', 'homebank', 'ofx', 'formats', 'utils']
