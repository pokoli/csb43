#!/usr/bin/awk -f

BEGIN{
    FS=":";
    OFS=":";
}
{
    if($4 ~ /^ (F|E[^2357])/){
        $4=" error:"$4;
    }else{
        $4=" warning:"$4;
    };
    print;
}
