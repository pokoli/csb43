.. csb43 Formats

:mod:`csb43.formats`
======================

.. automodule:: csb43.formats
    :members:
    :undoc-members:

.. autoattribute:: csb43.formats.FORMATS

.. autoattribute:: csb43.formats.TABLIB_FORMATS

.. autoattribute:: csb43.formats.DICT_FORMATS

.. autofunction:: csb43.formats.convertFromCsb

.. autofunction:: csb43.formats.convertFromCsb2Dict

.. autofunction:: csb43.formats.convertFromCsb2Tabular