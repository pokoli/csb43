.. csb43 API Documentation

.. contents:: :local:

:mod:`csb43.csb43`
====================

.. automodule:: csb43.csb43
    :members:

File -- (es) Fichero
-------------------------------------------------

.. autoclass:: csb43.csb43.File
    :members:
    :special-members:

.. autoclass:: csb43.csb43.ClosingFile
    :members:
    :special-members:

Account -- (es) Cuenta
-------------------------------------------------

.. autoclass:: csb43.csb43.Account
    :members:
    :special-members:

.. autoclass:: csb43.csb43.ClosingAccount
    :members:
    :special-members:

Transaction -- (es) Movimiento
-------------------------------------------------

.. autoclass:: csb43.csb43.Transaction
    :members:
    :special-members:

Item -- (es) Concepto
-------------------------------------------

.. autoclass:: csb43.csb43.Item
    :members:
    :special-members:

Exchange -- (es) Cambio de divisa
------------------------------------------------------

.. autoclass:: csb43.csb43.Exchange
    :members:
    :special-members:

Others
---------

.. autoclass:: csb43.csb43.RecordSequence
    :members:
    :special-members:

