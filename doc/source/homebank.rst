.. csb43 Homebank

:mod:`csb43.homebank`
======================

.. automodule:: csb43.homebank
    :members:

.. autoclass:: csb43.homebank.Transaction
    :members:
    :special-members:

Converter
----------

.. autofunction:: csb43.homebank.converter.convertFromCsb

.. autoattribute:: csb43.homebank.converter.PAYMODES
