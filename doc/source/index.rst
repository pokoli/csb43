.. csb43 documentation master file, created by

Welcome to csb43's documentation!
=================================

.. automodule:: csb43

API
====

Contents:

.. toctree::
    :maxdepth: 2

    csb
    ofx
    homebank
    formats
    utils

.. include:: converter.rst

Indices and tables
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

