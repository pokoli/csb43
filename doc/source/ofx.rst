.. csb43 Ofx

.. contents:: :local:

:mod:`csb43.ofx`
=================

.. automodule:: csb43.ofx
    :members:
    :inherited-members:

Converter
----------

.. autofunction:: csb43.ofx.converter.convertFromCsb

.. autoattribute:: csb43.ofx.converter.PAYMODES
