#!/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals

import pycountry

if __name__ == '__main__':

    print("# -*- coding: utf-8 -*-")
    print("from __future__ import unicode_literals")
    print("CURRENCY_DATA = [")
    for el in pycountry.currencies:
        alpha_3 = getattr(el, 'alpha_3', None)
        numeric = getattr(el, 'numeric', None)
        print("    %s," % ((alpha_3, numeric),))

    print("]")
    print()
